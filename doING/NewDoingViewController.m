//
//  NewDoingViewController.m
//  doING
//
//  Created by eugurlu on 25.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import "NewDoingViewController.h"
#import "DoingOperations.h"
#import "DBManager.h"
#import "MyDoingTableViewCell.h"
#import "Doing.h"
#import "NetworkManager.h"

@interface NewDoingViewController (){
    NSArray *myDoingArray;
    NSArray *myOperationArray;
} @end


@implementation NewDoingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    myOperationArray = [DoingOperations getAllOperations];
    myDoingArray = [[DBManager instance] listCustomerDoingFromDB:1];
    
    [[NetworkManager instance] getProductBalance:self];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    // If you're serving data from an array, return the length of the array:
    return [myDoingArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    MyDoingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    Doing *doing = [myDoingArray objectAtIndex:indexPath.row];
    
    [cell.triggerOperationIcon setImage:[[DoingOperations getOperationFromID:doing.triggeroperatinid.intValue] getImageFromID]];
    [cell.targetOperationIcon setImage:[[DoingOperations getOperationFromID:doing.targetoperationid.intValue] getImageFromID]];
    
    return cell;
}

- (void) returnedBalance:(NSString*) blc{
    NSLog(@"%@",blc);
}


@end
