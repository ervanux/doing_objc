//
//  MyDoingTableViewCell.h
//  doING
//
//  Created by eugurlu on 25.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDoingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *triggerOperationIcon;
@property (weak, nonatomic) IBOutlet UIImageView *targetOperationIcon;

@end
