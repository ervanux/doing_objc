//
//  Doing.m
//  doING
//
//  Created by mobdev on 10/25/13.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import "Doing.h"


@implementation Doing

@dynamic customerid;
@dynamic customerdoingid;
@dynamic triggeroperatinid;
@dynamic triggerparams;
@dynamic targetoperationid;
@dynamic targetparams;
@dynamic doingstatus;

@end
