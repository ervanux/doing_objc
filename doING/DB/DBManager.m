//
//  DBManager.m
//  doING
//
//  Created by mobdev on 10/25/13.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import "DBManager.h"
#import "Doing.h"


@implementation DBManager

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

static DBManager *obj = nil;    // static instance variable

+ (DBManager *)instance {
    if (obj == nil) {
        obj = [[super allocWithZone:NULL] init];
    }
    return obj;
}

- (void) commit{
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}

- (void) saveToDB:(int) customerid doingid:(int) customerdoingid triggerid:(int) triggeroperatinid triggerparameters:(NSString*) triggerparams targetid:(int) targetoperationid targetparameters:(NSString*) targetparams status:(int) doingstatus {
    Doing *trx = [NSEntityDescription insertNewObjectForEntityForName:@"Doing" inManagedObjectContext:self.managedObjectContext];
    trx.customerid = [NSNumber numberWithInt:customerid];
    trx.customerdoingid = [NSNumber numberWithInt:customerdoingid];
    trx.triggeroperatinid = [NSNumber numberWithInt:triggeroperatinid];
    trx.triggerparams = triggerparams;
    trx.targetoperationid = [NSNumber numberWithInt:targetoperationid];
    trx.targetparams = targetparams;
    trx.doingstatus = [NSNumber numberWithInt:doingstatus];
    
    
    [self commit];
}

- (NSArray *) listCustomerDoingFromDB:(int) customerid  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Doing" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(customerid=%@)", [NSNumber numberWithInt:customerid]];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
    
}

- (NSArray *) listBrowsFromDB{
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Doing" inManagedObjectContext:self.managedObjectContext];
//    [fetchRequest setEntity:entity];
//    [fetchRequest setReturnsDistinctResults:YES];
//    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:@"triggeroperatinid"]];
    
    
    //fetchRequest.resultType = NSDictionaryResultType;
    //fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"triggeroperatinid"]];
    //fetchRequest.returnsDistinctResults = YES;
    
//    fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"customerid"]];
//    fetchRequest.resultType = NSManagedObjectResultType;


    //[fetchRequest setReturnsDistinctResults:YES];
    
    
//    NSError *error;
//    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
//
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Doing" inManagedObjectContext:self.managedObjectContext];
    request.entity = entity;
    request.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"customerid"]];
    request.returnsDistinctResults = YES;
    request.resultType = NSManagedObjectResultType;
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"customerid" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSError *error = nil;
    NSArray *distincResults = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    
    return distincResults;
    
}


-(void) filtre{

    
}


- (void) updateStatus:(int) customerid doingid:(int) customerdoingid status:(int) doingstatus{
    
    
    NSArray *fetchArray = [self listCustomerDoingFromDB:customerid];
    
    for (Doing *trx1 in fetchArray) {
        if (trx1.customerdoingid.intValue==customerdoingid) {
            trx1.doingstatus = [NSNumber numberWithInt:doingstatus];
        }
    }
    
    [self commit];
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext {
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}











@end
