//
//  DBManager.h
//  doING
//
//  Created by mobdev on 10/25/13.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBManager : NSObject


@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (DBManager *)instance;
- (NSURL *)applicationDocumentsDirectory;
- (void) commit;
- (void) saveToDB:(int) customerid doingid:(int) customerdoingid triggerid:(int) triggeroperatinid triggerparameters:(NSString*) triggerparams targetid:(int) targetoperationid targetparameters:(NSString*) targetparams status:(int) doingstatus;
- (NSArray *) listCustomerDoingFromDB:(int) customerid;
- (NSArray *) listBrowsFromDB;
- (void) updateStatus:(int) customerid doingid:(int) customerdoingid status:(int) doingstatus;

@end
