//
//  Doing.h
//  doING
//
//  Created by mobdev on 10/25/13.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Doing : NSManagedObject

@property (nonatomic, retain) NSNumber * customerid;
@property (nonatomic, retain) NSNumber * customerdoingid;
@property (nonatomic, retain) NSNumber * triggeroperatinid;
@property (nonatomic, retain) NSString * triggerparams;
@property (nonatomic, retain) NSNumber * targetoperationid;
@property (nonatomic, retain) NSString * targetparams;
@property (nonatomic, retain) NSNumber * doingstatus;

@end
