//
//  NetworkManager.h
//  doING
//
//  Created by Başarı Kubuzcu on 10/25/13.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject
+ (NetworkManager *)instance;
- (void) login:(id) del;
- (void) getProductBalance:(id) del;
- (void) transferMoney:(id) del amount:(int)aoun;
@end
