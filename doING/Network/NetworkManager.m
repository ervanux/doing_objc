//
//  NetworkManager.m
//  doING
//
//  Created by Başarı Kubuzcu on 10/25/13.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import "NetworkManager.h"

@interface NetworkManager (){
    NSMutableData *responseData;
    //login
    NSString *cookieStr;
    NSString *ticketStr;
    NSString *nameStr;
    NSString *idStr;
    NSString *trxID;
    int transferAmoun;
    
    
    NSURLConnection *getTicketConnection;
    NSURLConnection *createSessionConnection;

    NSURLConnection *getProductConnection;
    NSURLConnection *getproductBalanceconnection;
    
    NSURLConnection *initiateTransfer;
    NSURLConnection *prepareForTransferConnection;
    NSURLConnection *updateTransferConnection;
    NSURLConnection *acceptTransferConnection;
    
    NSArray *products;
    
    
    id delegate;
    

    
}

@end


@implementation NetworkManager

static NetworkManager *obj = nil;    // static instance variable

+ (NetworkManager *)instance {
    if (obj == nil) {
        obj = [[super allocWithZone:NULL] init];
    }
    return obj;
}

- (void) login:(id) del{
    responseData = [[NSMutableData alloc] init];

    delegate=del;
    
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"https://apisandbox.ingdirect.es/openlogin/rest/ticket?apikey=Guf6NfdetBB5Dc6cIUjuR5zYzHzMqeB4"]];
    
    NSError *error;
    
    NSDictionary *JSON =
    [NSJSONSerialization JSONObjectWithData: [@"{\"loginDocument\": { \"documentType\": 0, \"document\": \"4790243X\" },\"birthday\": \"01/01/1980\" }"
                                              dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: &error];
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:JSON options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    getTicketConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [getTicketConnection start];

}

- (void) createSession{
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"https://apisandbox.ingdirect.es/openapi/login/auth/response?apikey=Guf6NfdetBB5Dc6cIUjuR5zYzHzMqeB4"]];
    
    
    NSString *content = [NSString stringWithFormat:@"ticket=%@",ticketStr];
    NSData *postData = [content dataUsingEncoding:NSUTF8StringEncoding];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    createSessionConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [createSessionConnection start];
    
}

- (void) getProduct{
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"https://apisandbox.ingdirect.es/openapi/rest/products?apikey=Guf6NfdetBB5Dc6cIUjuR5zYzHzMqeB4"]];
    
    
    [request setValue:cookieStr forHTTPHeaderField:@"Cookie"];
    [request setHTTPMethod:@"GET"];
    
    getProductConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [getProductConnection start];
    
}

- (void) transferMoney:(id) del amount:(int)aoun{
    transferAmoun = aoun;
    [self prepareForTransfer];
    
}

- (void) prepareForTransfer{
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"https://apisandbox.ingdirect.es/openapi/rest/prepare-movemoney?apikey=Guf6NfdetBB5Dc6cIUjuR5zYzHzMqeB4&operatives=10"]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:cookieStr forHTTPHeaderField:@"Cookie"];
    prepareForTransferConnection =[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [prepareForTransferConnection start];
}

- (void) initiateTransfer{
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"https://apisandbox.ingdirect.es/openapi/rest/transfers?apikey=Guf6NfdetBB5Dc6cIUjuR5zYzHzMqeB4"]];
    
    [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:cookieStr forHTTPHeaderField:@"Cookie"];
    [request setHTTPMethod:@"POST"];
    initiateTransfer =[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [initiateTransfer start];
    
    
}

- (void) updateTransfer:(NSString*) transid{
    trxID = transid;
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://apisandbox.ingdirect.es/openapi/rest/transfers/%@?apikey=Guf6NfdetBB5Dc6cIUjuR5zYzHzMqeB4",transid]]];
    
    NSDictionary *productFrom = [products objectAtIndex:0];
    NSDictionary *productTo = [products objectAtIndex:1];
    
    NSString *toName = [[[productTo objectForKey:@"holders"] objectAtIndex:0] objectForKey:@"name"];
    
    NSString *productFromNumber = [productFrom objectForKey:@"productNumber"];
    NSString *productToNumber = [productTo objectForKey:@"productNumber"];
    
    NSError *error;
    
    NSDictionary *JSON =
    [NSJSONSerialization JSONObjectWithData: [[NSString stringWithFormat:@"{\"from\": {\"productNumber\": \"%@\"},\"to\": {\"productNumber\": \"%@\",\"titular\": \"%@\"},\"amount\": %d,\"currency\": \"EUR\",\"operationDate\": \"25/10/2013\",\"concept\": \"ING Direct Spain Movement\"}",productFromNumber,productToNumber,toName,transferAmoun]
                                              dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: &error];
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:JSON options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"PUT"];
    [request setHTTPBody:postData];
    updateTransferConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [updateTransferConnection start];

}

- (void) acceptTransfer:(NSString*) acceptValue{
    NSString *urlStr = [NSString stringWithFormat:@"https://apisandbox.ingdirect.es/openapi/rest/transfers/%@/accept?apikey=Guf6NfdetBB5Dc6cIUjuR5zYzHzMqeB4",trxID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    NSError *error;
    
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [[NSString stringWithFormat:@"{\"acceptanceValue\":\"%@\"}",acceptValue]
                                              dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: &error];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:JSON options:0 error:&error];
    [request setValue:cookieStr forHTTPHeaderField:@"Cookie"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"PUT"];
    [request setHTTPBody:postData];
    acceptTransferConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [acceptTransferConnection start];

}

- (void) getProductBalance:(id) del{
    delegate = del;
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"https://apisandbox.ingdirect.es/openapi/rest/products?apikey=Guf6NfdetBB5Dc6cIUjuR5zYzHzMqeB4"]];
    
    
    [request setValue:cookieStr forHTTPHeaderField:@"Cookie"];
    [request setHTTPMethod:@"GET"];
    
    getproductBalanceconnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [getproductBalanceconnection start];
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    if ([connection isEqual: createSessionConnection]) {
        NSDictionary *headers = [(NSHTTPURLResponse *)response allHeaderFields];
        cookieStr = [headers objectForKey:@"Set-Cookie"];
    }
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {

    
    if ([connection isEqual: getTicketConnection]) {
        NSLog(@"Ticket Connection  Finished");
//        NSLog(@"\n\n\nResponse data:\n %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        if ([jsonDict objectForKey:@"ticket"]) {
            ticketStr= [jsonDict objectForKey:@"ticket"];
            [self performSelector:@selector(createSession) withObject:nil afterDelay:1];
        }
    } else if ([connection isEqual: createSessionConnection]){
        NSLog(@"Create Session Connection  Finished");
//        NSLog(@"\n\n\nResponse data:\n %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        [self performSelector:@selector(getProduct) withObject:nil afterDelay:1];
    } else if ([connection isEqual: getProductConnection]){
        NSLog(@"Product Connection  Finished");
//        NSLog(@"\n\n\nResponse data:\n %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        if ([jsonArray count]) {
            products= jsonArray;
        }
    }else if ([connection isEqual:prepareForTransferConnection]){
        NSLog(@"Prepare For Transfer Connection Finished");
//        NSLog(@"\n\n\nResponse data:\n %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        [self performSelector:@selector(initiateTransfer) withObject:nil afterDelay:1];
    }else if ([connection isEqual:initiateTransfer]){
        NSLog(@"Inıtiate Transfer Connection  Finished");
//        NSLog(@"\n\n\nResponse data:\n %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        NSDictionary *jsonDict=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        if ([jsonDict objectForKey:@"id"]) {
            [self performSelector:@selector(updateTransfer:) withObject:[jsonDict objectForKey:@"id"] afterDelay:1];
        }
    }else if ([connection isEqual:getproductBalanceconnection]){
        NSLog(@"Product Balance Connection Finished");
//        NSLog(@"\n\n\nResponse data:\n %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        if ([jsonArray count]) {
            NSDictionary *jsonDict= [jsonArray objectAtIndex:0];
            if ([jsonDict objectForKey:@"availableBalance"]) {
                NSString *availableBalance= [jsonDict objectForKey:@"availableBalance"];
                [delegate performSelector:@selector(returnedBalance:) withObject:availableBalance afterDelay:1];
            }
                
        }
    }else if([connection isEqual:updateTransferConnection]){
        NSLog(@"Update Transfer Connection Finished");
//        NSLog(@"\n\n\nResponse data:\n %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        if ([jsonDict objectForKey:@"acceptanceMethod"]) {
            NSDictionary *jsonDict2 = [jsonDict objectForKey:@"acceptanceMethod"];
            
            if ([jsonDict2 objectForKey:@"pinPositions"]) {
                NSArray *pinPositions= [jsonDict2 objectForKey:@"pinPositions"];
                if ([pinPositions count]>1) {
                    NSString *acceptanceValue = [NSString stringWithFormat:@"%@,%@",[pinPositions objectAtIndex:0],[pinPositions objectAtIndex:1]];

                    [self performSelector:@selector(acceptTransfer:) withObject:acceptanceValue afterDelay:1];

                }
            }
        }
    }else if([connection isEqual:acceptTransferConnection]){
        NSLog(@"Accept Transfer Connection Finished");
//        NSLog(@"\n\n\nResponse data:\n %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        if ([jsonDict objectForKey:@"transferTransaction"]) {
            
        }
        
    } else{
        NSLog(@"Other Connection  Finished");
        //NSLog(@"\n\n\nResponse data:\n %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    }
    
    [responseData setLength:0];    

}


@end
