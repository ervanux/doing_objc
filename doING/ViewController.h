//
//  ViewController.h
//  doING
//
//  Created by eugurlu on 24.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BButton.h"
#import "BaseViewController.h"

@interface ViewController : BaseViewController
@property (weak, nonatomic) IBOutlet BButton *loginBtn;
@property (weak, nonatomic) IBOutlet BButton *loginIngConnectBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)loginPressed:(id)sender;

@end
