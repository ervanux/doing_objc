//
//  DoingOperations.h
//  doING
//
//  Created by eugurlu on 25.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoingOperations : NSObject

@property (nonatomic, assign) int operationID;
@property (nonatomic, retain) NSString * iconName;

+ (NSArray*) getAllOperations;
- (UIImage*) getImageFromID;
+ (DoingOperations*) getOperationFromID:(int) did;
@end
