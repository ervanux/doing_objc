//
//  main.m
//  doING
//
//  Created by eugurlu on 24.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
