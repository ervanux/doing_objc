//
//  ViewController.m
//  doING
//
//  Created by eugurlu on 24.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import "ViewController.h"
#import "NetworkManager.h"
#import "DashboardViewController.h"

@interface ViewController (){
    NSString *nameStr;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
//    [self.loginBtn setType:BButtonTypeSuccess];
//    [self.loginIngConnectBtn setType:BButtonTypePrimary];
//    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Login.png"]]];

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginPressed:(id)sender {
//    [[NetworkManager instance] login:self];
    [self performSegueWithIdentifier:@"logintToDashboard" sender:self]; //Comment out this
}

-(void) returnedName:(NSString*) name{
    nameStr = name;
    [self performSegueWithIdentifier:@"logintToDashboard" sender:self];
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"logintToDashboard"]){
        DashboardViewController *dashboard = segue.destinationViewController;
        [dashboard objectFromLoginView:nameStr];
    }
    
}

@end
