//
//  AppDelegate.h
//  doING
//
//  Created by eugurlu on 24.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
