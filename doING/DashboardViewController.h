//
//  DashboardViewController.h
//  doING
//
//  Created by eugurlu on 25.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface DashboardViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;


- (void) objectFromLoginView:(NSString*) str;

@end
