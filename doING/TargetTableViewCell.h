//
//  TargetTableViewCell.h
//  doING
//
//  Created by eugurlu on 25.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TargetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;

@end
