//
//  DoingOperations.m
//  doING
//
//  Created by eugurlu on 25.10.2013.
//  Copyright (c) 2013 eugurlu. All rights reserved.
//

#import "DoingOperations.h"

@implementation DoingOperations

static NSArray * allOperations;

+ (NSArray*) getAllOperations{
    if (allOperations == nil) {
        
        DoingOperations *accountBalance= [[DoingOperations alloc] init];
        accountBalance.iconName= @"OperationIcon_accountBalance.png";
        accountBalance.operationID = 1;

        
        DoingOperations *transferMoney= [[DoingOperations alloc] init];
        transferMoney.iconName= @"OperationIcon_transferMoney.png";
        transferMoney.operationID = 2;

        allOperations = [[NSArray alloc] initWithObjects:accountBalance,transferMoney,nil];
        
    }
    
    return allOperations;
}


+ (DoingOperations*) getOperationFromID:(int) did{
    NSArray *array = [DoingOperations getAllOperations];
    
    for (DoingOperations *doing in array) {
        if (doing.operationID==did) {
            return doing;
        }
    }
    
    return nil;
}

- (UIImage*) getImageFromID{
    return [UIImage imageNamed:self.iconName];
}


@end
